# Utiliser une image de base contenant Python
FROM python:3.9-slim

# Installer Jupyter Notebook et autres dépendances
RUN pip install notebook pandas scikit-learn joblib

# Copier votre notebook dans le conteneur
COPY flask.ipynb /app/

# Exposer le port 8888 pour Jupyter Notebook
EXPOSE 8888

# Commande pour démarrer Jupyter Notebook
CMD ["jupyter", "notebook", "--ip=0.0.0.0", "--port=8888", "--allow-root", "--NotebookApp.token=''"]