# exam_cicd - Projet Machine Learning avec Flask, DVC et CI/CD

Ce projet est une application Flask utilisant un modèle de Machine Learning pour prédire des sentiments à partir de critiques de films. Il intègre également Data Version Control (DVC) pour le suivi des versions des données et un pipeline CI/CD avec GitLab pour automatiser le déploiement.

## Installation

1. Cloner le repository :

   ```bash
   git clone https://example.com/votre-repository.git
